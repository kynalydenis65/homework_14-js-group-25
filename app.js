    const changeThemeButton = document.querySelector('.change_theme');
    const body = document.body;

    // Перевірте, чи вже встановлено вибір теми користувача
    if (localStorage.getItem('theme') === 'dark') {
    body.classList.add('dark-theme');
}

    changeThemeButton.addEventListener('click', () => {
    // Перемикайтеся між світлою і темною темами
    if (body.classList.contains('dark-theme')) {
    body.classList.remove('dark-theme');
    localStorage.setItem('theme', 'light');
} else {
    body.classList.add('dark-theme');
    localStorage.setItem('theme', 'dark');
}
});
